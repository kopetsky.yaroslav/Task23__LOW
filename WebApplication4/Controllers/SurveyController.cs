﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class SurveyController : Controller
    {
        // GET: Survey
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string Post(FormCollection collection)
        {
            bool check = Convert.ToBoolean(collection["Like"].Split(',')[0]);

            string Result = "POSTED DATA:\n " + "Liked design:" + check +"\n" + "Selected Author: "+collection["Drop"];
            return Result;
        }
    }
}